import Vue from 'vue';
import Router from 'vue-router';
import Home from './pages/Home.vue';
import Converter from './components/Converter.vue';
import Time from './components/Time.vue';
import serverStatus from './pages/serverStatus';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/converter',
      name: 'converter',
      component: Converter,
    }, {
      path: '/time',
      name: 'time',
      component: Time,
    },{
      path: '/serverStatus',
      name: 'server status',
      component: serverStatus,
    }
  ],
});
