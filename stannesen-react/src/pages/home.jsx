import React, { Component } from 'react';

class Home extends Component {
	constructor(){
		super();
		this.state = {
			err: null,
      isLoaded: false,
      posts: [],
		}
	}

	componentDidMount(){
		this.getJson();

		if(this.state.isLoaded === true){
			console.log("Hey");
		}
	}

	getJson(){
		//let channel = this.props.channel.handle;
    fetch("https://europe-west1-stannesen-a38ec.cloudfunctions.net/widgets/all")
      .then(response => response.json())
      .then(jsonData => {
        this.setState({
          isLoaded: true,
          posts: jsonData.channels,
				});
      })
      .catch(error => {
        this.setState({
          isLoaded: true,
          error
        });
      });
	}

	render() {
		return (
			<div className="">
				<h1 className="info__header">Home</h1>
			</div>
		);
	}
}

export default Home;