// Importing packages
import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from '../pages/home';

// Importing CSS(SCSS)
import '../css/global.scss';

// Importing components
import Header from '../components/header.jsx';
import Footer from '../components/footer.jsx';
import TicTacToe from '../components/Tic-tac-toe.jsx';
import YoutubeInfo from './youtube/youtubeInfo.jsx'
import YoutubeChannels from './youtube/youtubeChannels.jsx';

// Adding router
const AppRouter = () => (
	<Router>
		<main className="">
			<Route path="/" exact component={Home} />
			<Route path="/tic-tac-toe/" component={TicTacToe} />
			<Route path="/youtube-info/:handle" component={YoutubeInfo} />
			<Route path="/youtube-channels" component={YoutubeChannels} />
		</main>
	</Router>
);

class app extends Component {
	render() {
		return (
			<div className="background">
				<div className="layer">
					<Header/>
					<main className="content">
						<AppRouter></AppRouter>
					</main>
					<Footer></Footer>
				</div>
			</div>

		);
	}
}

export default app;