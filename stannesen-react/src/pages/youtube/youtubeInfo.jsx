// Importing packages
import React, { Component } from 'react';

// Import components
import DisplayYoutubeInfo from '../../components/youtube/displayYoutubeInfo.jsx';

class youtubeInfo extends Component {
	// Render the component to show youtube channel info
	render() {
		return (
			<div className="info__holder">
				<DisplayYoutubeInfo channel={this.props.match.params}></DisplayYoutubeInfo>
			</div>
		);
	}
}

export default youtubeInfo;