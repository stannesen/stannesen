// Importing packages
import React, { Component } from 'react';

// Import components
import DisplayYoutubeChannels from '../../components/youtube/displayYoutubeChannels.jsx';

class youtubeChannels extends Component {
	render() {
		return (
			<div className="info__holder--colums">

				<h1 className="info__header">Youtube Channels</h1>
				<DisplayYoutubeChannels></DisplayYoutubeChannels>

			</div>
		)
	}
}

export default youtubeChannels;