// Importing packages
import React from "react";
import CurrentVideo from "./currentVideo.jsx";
import Linkify from 'react-linkify';


// Importing CSS(SCSS)
import "../../css/youtube.scss";

class displayYoutubeInfo extends React.Component {
  constructor() {
    super();
    this.state = {
      err: null,
      isLoaded: false,
      posts: [],
      currentChannel: "",
      videoToShow: "",
      showDescription: false,
    };
  }

  // Fetching the item it needs from the API
  // The channel it is going to show.
  componentDidMount() {
    let channel = this.props.channel.handle;
    fetch("https://europe-west1-stannesen-a38ec.cloudfunctions.net/widgets/all/" + channel)
      .then(response => response.json())
      .then(jsonData => {
        this.setState({
          isLoaded: true,
          posts: jsonData,
          currentChannel: channel,
          videoToShow: this.getVideo(jsonData, channel),
        });
      })
      .catch(error => {
        this.setState({
          isLoaded: true,
          error
        });
      });
  }

  // Returns the video it is going to show. The first video n
  getVideo(jsonData, channel) {
    let __temp = "yt:videoId";
    let videoID = jsonData.channels[channel].json.feed.entry[0][__temp];
    return videoID;
  }

  // Returns the list of latest videos
  creatVideoList(item) {
    let __tempchannelList = item.channels[this.state.currentChannel].json.feed.entry;
    let videoID = "yt:videoId";

    let __tempList = __tempchannelList.map((item, index) => (
      <div key={index} className="grid__item">
        <button
          onClick={() => this.videoToShow(item)}
          key={index}
          className="thumbnails"
        >
          <div className="thumbnails__holder">
            <img
              className="thumbnails__img"
              src={"https://img.youtube.com/vi/" + item[videoID] + "/maxresdefault.jpg"}
              alt=""
            />
          </div>
        </button>
        <h3 className="info__video-title">{this.unEntity(item.title)}</h3>
        <div className="info__video-text">published: {this.returnDate(item.published)}</div>
      </div>
    ));

    return __tempList;
  }

  retrunVideoInfo(item) {
    let __tempchannelList = item.channels[this.state.currentChannel].json.feed.entry;
    let videoID = "yt:videoId";
    let temp = "";

    __tempchannelList.forEach(element => {
      if (element[videoID] === this.state.videoToShow) {
        temp = element;
      }
    });
    return temp;
  }


  returnTextWithLineBread(item) {
    let __tempMediaGroup = "media:group";
    let __tempMediaDescription = "media:description";
    let text = this.retrunVideoInfo(item)[__tempMediaGroup][__tempMediaDescription];

    let newText = text.split('\n').map((item, i) => {
      return <span key={i}>{this.unEntity(item)} <br/></span>;
    });
    return newText;
  }

  // Setting the first video to show
  // The first vide is the latest onee.
  videoToShow(item) {
    let __tmpeVideoID = "yt:videoId";
    let video = item[__tmpeVideoID];
    this.setState({
      videoToShow: video
    });

    this.topFunction();
  }

  returnDate(date) {
    let newDate = date.split("T", 1);
    return newDate;
  }

  topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  toggleDescription(item){
    
    console.log("test: ", item)
    this.setState(prevState => ({
      showDescription: !prevState.showDescription
    }));
  }

  returnClassName(posts){
    if(this.state.showDescription){
      return (
        <div className="description__info description__info--show" id="descritionText">
          <Linkify properties={{target: '_blank', style: {color: 'white', fontWeight: 'bold', width: "160vh"}}}>
            {this.returnTextWithLineBread(posts)}
          </Linkify>
        </div>
      )
    }else{
      return (
        <div className="description__info description__info--hidden" id="descritionText">
          <Linkify properties={{target: '_blank', style: {color: 'white', fontWeight: 'bold', width: "160vh"}}} >
            {this.returnTextWithLineBread(posts)}
          </Linkify>
        </div>
      )
    }
  }

  unEntity(str){
    return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"');
 }

  // Render info about the channel
  render() {
    const { error, isLoaded, posts, currentChannel } = this.state;
    const __tempVideoID = 'yt:videoId';

    if (error) {
      return <div className="error">Error in loading</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      let videoLink = posts.channels[currentChannel].json.feed;

      return (
        <div className="info">
          <h1 className="info__header">
            {this.unEntity(posts.channels[currentChannel].json.feed.title)}
          </h1>
          <div className="info__box">
            <h1 className="info__title"> {this.unEntity(this.retrunVideoInfo(posts).title)}</h1>
            <div className="info__top">
              <CurrentVideo video={this.state.videoToShow} />
              <div className="info__cnt">
                <h3>Published: {this.returnDate(this.retrunVideoInfo(posts).published)}</h3>
                <a className="info__link" href={"https://www.youtube.com/watch?v=" + this.retrunVideoInfo(posts)[__tempVideoID]}>Link to Video</a>
                <h3 className="description__header">Description</h3>
                <div className="description">
                  {this.returnClassName(posts)}
                  <button onClick={() => this.toggleDescription(this.state.showDescription)} 
                    className="description__button">{this.state.showDescription ? "...Show Less..." : "...Show More..."}</button>
                  <p>
                    {this.state.showDescription}
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div>
            <h1 className="info__header">Latest videos</h1>
            <div className="info__ol--holder">
              <ol className="info__ol--grid">{this.creatVideoList(posts)}</ol>
            </div>
          </div>
          <div>
            <h1 className="info__header">Channel info</h1>
            <div className="info__box">
              <p>Name: {this.unEntity(videoLink.title)}</p>
              <p>Channel created: {this.returnDate(videoLink.published)}</p>
              <a className="info__link" href={videoLink.author.uri}>{videoLink.author.uri}</a>
            </div>
          </div>
        </div>
      );
    }
  }
}

export default displayYoutubeInfo;
