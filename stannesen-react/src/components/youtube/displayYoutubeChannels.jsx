// Importing packages
import React, { Component } from "react";
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button';


// Importing Style
import "../../css/youtube.scss";
import { withStyles } from "@material-ui/core";


const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

class retunrJson extends Component {
  constructor() {
    super();
    this.state = {
      error: null,
      isLoaded: false,
      posts: [],
    };
  }


  unEntity(str) {
    return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"');
  }

  // This Function runs when the component is loaded
  // Fetching JSON from my API
  componentDidMount() {
    fetch("https://europe-west1-stannesen-a38ec.cloudfunctions.net/widgets/all")
      .then(response => response.json())
      .then(jsonData => {
        this.setState({
          isLoaded: true,
          posts: jsonData
        });
      })
      .catch(error => {
        this.setState({
          isLoaded: true,
          error
        });
      });
  }

  // Creates the list that holds youtube channels
  createList(items) {
    try {
      let listLength = [].concat(Object.keys(items.channels))
        .sort()
        .map((item, index) => (
          <Button classes={{ label: 'grid__item grid__item--channels' }} component={Link} to={"/youtube-info/" + item} key={index}>
            {this.unEntity(items.channels[item].json.feed.title)}
          </Button>
        ));
      return listLength;
    }
    catch (error) {
      console.log(error)
      return (
        <div>
          <h1>Error in loading</h1>
        </div>
      )
    }

  }


  // Render channels grid
  render() {
    const { error, isLoaded, posts } = this.state;

    if (error) {
      return <div className="error">Error in loading</div>;
    } else if (!isLoaded) {
      return <div>Loading ...</div>;
    } else {
      return (
        <div className="grid__holder">
          <div className="grid">
            <ul className="grid__ul">{this.createList(posts)}</ul>
          </div>
        </div>
      );
    }
  }
}

export default withStyles(styles)(retunrJson);
