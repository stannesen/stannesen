// Importing packages
import React from "react";


class currentVideo extends React.Component {
  constructor() {
    super();
    this.state = {
      currentVideo: "",
      videoInfo: {},
      isLoaded: false,
    };
  }

  _element = React.createRef();

  // This Function runs when the component is loaded
  componentDidMount() {
    if (this.state.currentVideo !== this.props.video) {
      this.setState({
        currentVideo: this.props.video,
        videoInfo: this.props.videoInfo,
        isLoaded: true,
      });
    }
  }

  // This Function runs when currentVideo get a new prop
  // Se if video selected is the one showing
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.video !== nextState.currentVideo) {
      this.setState({
        currentVideo: nextProps.video,
        isLoaded: true
      });
    }else if(nextProps.videoInfo !== nextState.videoInfo){
      this.setState({
        videoInfo: nextProps.videoInfo,
        isLoaded: true,
      })
    }

    // Check if it done loading image
    if (nextState.isLoaded) {
      return true;
    } else {
      return false;
    }
  }

  // Render the youtube player
  render() {
    const { currentVideo, isLoaded } = this.state;

    // Loading cheack.
    // Show loading text if it is not done loading
    if (!isLoaded) {
      return <div className="error">Loading...</div>;
    } else {
      return (
        <div className="youtube">
            <div className="youtube__player" >
              <iframe title="youtubeVideos" className="youtube__player--iframe" src={"https://www.youtube.com/embed/" + currentVideo} frameBorder="0"></iframe>
            </div>
        </div>
      );
    }
  }
}

export default currentVideo;
