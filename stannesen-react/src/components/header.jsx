import React, { Component } from "react";
import Button from "@material-ui/core/Button";

import "../css/header.scss";


class header extends Component {
  constructor() {
    super();
    this.state = {
      mobileMenu: false,
      youtubeMenu: false,
    }
  }

  toggleYoutubeMenu() {
    this.setState((state) => ({
      youtubeMenu: !state.youtubeMenu
    }))
  }

  toggleMobileMenu() {
    this.setState((state) => ({
      mobileMenu: !state.mobileMenu
    }))
  }

  shouldComponentUpdate(nextState) {
    if (this.state.mobileMenu !== nextState.mobileMenu) {
      return true;
    } else {
      return false;
    }
  }

  showMobileMenu() {
    return (
      <div className="mobile-menu" id="mobile-menu">
        <ul className="mobile-menu__ul">
          <li className="mobile-menu__li">
            <Button classes={{ label: 'mobile-menu__button--left' }} className="mobile-menu__button" href="/" onClick={() => this.toggleMobileMenu()}>Home</Button>
          </li>
          <li className="mobile-menu__li">
            <Button
              classes={{ label: 'mobile-menu__button--left' }}
              className="mobile-menu__button mobile-menu__li--folder"
              onClick={() => this.toggleYoutubeMenu()}>
              <p className="mobile-menu__text">Youtube</p>
              <p className="mobile-menu__text--end">▼</p>
            </Button>
            {this.state.youtubeMenu &&
              <div>
                <ul className="mobile-menu__ul--under">
                  <li className="mobile-menu__li--under">
                    <Button
                      classes={{ label: 'mobile-menu__button--left' }}
                      className="mobile-menu__button mobile-menu__li--folder"
                      href="/youtube-channels"
                      onClick={() => this.toggleMobileMenu()}>
                      Channels
                    </Button>
                  </li>
                  {/* <li className="mobile-menu__li--under">
                    <Button
                      classes={{ label: 'mobile-menu__button--left' }}
                      className="mobile-menu__button mobile-menu__li--folder"
                      onClick={() => this.toggleMobileMenu()}>
                      Top videos
                    </Button>
                  </li> */}
                </ul>
              </div>
            }
          </li>
          <li className="mobile-menu__li">
            <Button
              classes={{ label: 'mobile-menu__button--left' }}
              className="mobile-menu__button"
              href="https://me.stannesen.com"
              onClick={() => this.toggleMobileMenu}>
              About Me
            </Button>
          </li>
        </ul>
        <div className="mobile-menu--outside" onClick={() => {this.toggleMobileMenu(); this.topFunction();}}></div>
      </div>
    )
  }

  topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  getButtonClass() {
    if (this.state.mobileMenu === true) {
      return "mobile-menu--btn change";
    } else {
      return "mobile-menu--btn";
    }
  }

  render() {
    return (
      <header className="header">

        
        <div className="header__holder">
          <button className={this.getButtonClass()} onClick={() => this.toggleMobileMenu()}>
            <div className="bar1"></div>
            <div className="bar2"></div>
            <div className="bar3"></div>
          </button>
          <div className="header__cnt">
            <a href="/" className="logo">Stannesen</a>
          </div>
          <div className="menu">
            <ul className="menu__ul">
              <li className="menu__li">
                <Button classes={{ label: 'button' }} color="primary" href="/youtube-channels">Youtube Channels</Button>
              </li>
            </ul>
          </div>
        </div>
        {this.state.mobileMenu && this.showMobileMenu()}
      </header>
    );
  }
}

export default (header);
