import React, { Component } from 'react';

import '../css/footer.scss'

class footer extends Component {
	render() {
		return (
			<footer className="footer">
				<div className="links">
					<div className="links__types">
						<p className="links__header">Links:</p>
						<ul className="footer__ul">
							<li className="footer__list">
								<a className="footer__link" href="https://me.stannesen.com/">About me</a>
							</li>
						</ul>
					</div>

					<div className="links__types">
						<p className="links__header">Youtube:</p>
						<ul className="footer__ul">
							<li className="footer__list">
								<a className="footer__link" href="/youtube-channels">Channels</a>
							</li>
						</ul>
					</div>
				</div>
				<h1 className="footer__logo">Stannesen Copyright © 2018-2019. All rights reserved.</h1>
			</footer>
		);
	}
}

export default footer;