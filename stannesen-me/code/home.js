let contactDiv = document.getElementById("contact");
let infoDiv = document.getElementById("info");
let defaultDiv = document.getElementById("default");
let mobileMenu = document.getElementById("mobile-menu");

window.addEventListener('resize', function(){
	if(screen.width > 500){
		mobileMenu.style.opacity = "0";
	}
});

function eventHandler(event) {
	console.log(event);
}

function toggleMobileList(){
 if(!mobileMenu.isVisible){
	mobileMenu.style.opacity = "1";
	mobileMenu.isVisible = true;
 }else{
	 mobileMenu.style.opacity = "0";
	 mobileMenu.isVisible = false;
 }
}

function hideMobileMenu(){
	mobileMenu.style.opacity = "0";
	mobileMenu.isVisible = false;
}

function toggleDefault() {
	if (contactDiv.isVisible || infoDiv.isVisible) {
		defaultDiv.style.opacity = "0";
	} else {
		defaultDiv.style.opacity = "1";
	}
}

function toggleContact() {
	hideMobileMenu();
	if (!contactDiv.isVisible) {
		contactDiv.style.opacity = "1";
		contactDiv.isVisible = true;
	} else {
		contactDiv.style.opacity = "0";
		contactDiv.isVisible = false;
	}

	if (infoDiv.isVisible) {
		infoDiv.style.opacity = "0";
		infoDiv.isVisible = false;
	}
	toggleDefault();
}

function toggleInfo() {
	hideMobileMenu();
	if (!infoDiv.isVisible) {
		infoDiv.style.opacity = "1";
		infoDiv.isVisible = true;
	} else {
		infoDiv.style.opacity = "0";
		infoDiv.isVisible = false;
	}

	if (contactDiv.isVisible) {
		contactDiv.style.opacity = "0";
		contactDiv.isVisible = false;
	}
	toggleDefault();
}